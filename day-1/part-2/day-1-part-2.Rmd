---
title:    "Working with Data, Plotting"
subtitle: "<small>*DS Toolbox, Day 1*</small>"
author:   "<small>@kevin_kunzmann</small>"
date:     "`r Sys.Date()`"
output: 
    revealjs::revealjs_presentation:
        css:            '../../resources/styles.css'
        self_contained: false
        reveal_plugins: ["notes"]
        reveal_options:
            slideNumber: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
    echo    = TRUE,
    comment = NA,
    prompt  = TRUE
)

library(tidyverse)

# load data
tbl_wb_hnp <- read_csv("../../resources/data/tbl_wb_hnp.csv")

# credentials for gcp
service_account_file_path <- '../../resources/service-account.json'
gcp_project_id <- jsonlite::read_json(
        '../../resources/service-account.json'
    )$project_id
```



## Working with Data, Plotting

**Before lunch**

* What is the `tidyverse` and why should I care?
* Data import with 'readr'
* Data 'wrangling' with 'dplyr' + 'tidyr'
* Systematic plotting with `ggplot2`

**After Lunch**

* Connecting to a database (Google BigQuery)
* Querying REst APIs
* Functional programming with 'purrr'



# Databases 101

<img src="../../resources/figures/tidy_db.png" style="float:left; width:37%; margin-right:5%; margin-bottom:0.5em;">
<img src="../../resources/figures/data-center.jpg" style="float:right; width:40%; margin-right:5%; margin-bottom:0.5em;">
<p style="clear: both;">



## Technically, this is a database!

<img src="../../resources/figures/messy_db.jpg" style="float:middle; width:55%; margin-right:5%; margin-bottom:0.5em;">
<p style="clear: both;">

* 'database' not well-defined
* anything from a scratchbook to Google data-center



## Databases - why bother at all?

<p align="left"> 'proper' databases ... </p>

* link multiple tables via common indices

* scale to 'big data' (individual files do not!)

* are mostly accessed remotely - **internet connection can become bottleneck** for sending or receiving data!

* expose a well-defined querying language/interface


$\leadsto$ data scientists need to know **how to query data**



## A practitioners perspective on databases

* databases are complicated!

* no need to understand the underpinnings!

* 99% of 'big data' problems only require a small fraction of the overall dataset

* subset often manageable with 'traditional' tools (no Apache Spark required)

> Applied data scientists only need to know how to get/query data!



##  `SQL` vs. **NoSQL** databases

* **SQL** (Structured Query Languge)
    * efficient at storing tabular data
    * queried using `SQL`
    * unfortunately: different `SQL` dialects
    * difficult to store documents/large binary files (images!)
    
* **NoSQL** 
    * everything else, flexible
    * can be geared towards special requirements 
    * combine different types of data
    * essentially key-value stores (think JSON!)
    * often queried via **REST API** (web)



## Structured Query Language

* quite old, 1974

* **linguga franca** of table databases, worth learning!

* some might be familiar with `SQL` through `SAS Proc SQL`

* simple example:
```SQL
SELECT var1 var2 FROM table WHERE var3 > 0;
```

* **complex queries can become tedious** to write/read




## Tidyverse & databases

```SQL
SELECT var1 var2 FROM table WHERE var3 > 0;
```
```r
table %>% 
    filter(var3 > 0) %>% 
    select(var1, var2)
```

* `dplyr` (via `dbplyr`) translates as much as possible to `SQL`

> same interface for files/databases and process as much as possible remotely before actually downloading data



## Our first (local) database

* create `SQLite` database in memory (locally)
```{r}
con <- DBI::dbConnect(RSQLite::SQLite(), dbname = ":memory:")
```

* add a copy of the World Bank data table
```{r}
copy_to(con, tbl_wb_hnp) # local copy of worldbank data
DBI::dbListTables(con)
```




## `dbplyr`'s automatic `SQL` generation

```{r}
qry <- tbl(con, 'tbl_wb_hnp') %>%
    filter(
        country_code %in% c('USA', 'CHN'),
        indicator_name == "Population, total"
    ) %>%
    select(country_name, indicator_name, year, value) 
show_query(qry)
```

* due to `SQL` dialects and R being more powerful, only works for simple queries
(no custom functions etc.)

* beware of `SQL` type conversions (`NA` $\leadsto$ 0 etc.)

* https://dbplyr.tidyverse.org/articles/sql-translation.html



## Remote vs. local data tables

```{r}
print(qry, n = 3)
print(collect(qry), n = 3)
```



# World Bank Data via BigQuery

* will work with original World Bank data 

* stored in Google Cloud Platform (GCP)

* access via Google BigQuery - 'the real thing' 

* GCP monthly free quota: 1Tb query volume!

* only one table, only <300Mb - peanuts!

* `bigrquery` implements database interface for `DBI` package (backend for `dbplyr`) 

* **works with `dplyr` out of the box!**



## Authenticating with Google Cloud Platform

```{r}
library(tidyverse)
library(bigrquery)
library(DBI)

bigrquery::bq_auth(path = service_account_file_path)
```

* need a 'service acount' and key file (.json!) for [Google Cloud Platform account](https://cloud.google.com/)

* just point to `service-account.json` file

* can also authenticate interactively, only required once one a machine (credentials are saved)



## Connecting to a database

```{r}
con <- dbConnect(
    bigrquery::bigquery(),
    project = "bigquery-public-data",
    dataset = "world_bank_health_population",
    billing = gcp_project_id # your billing project goes here
)
```

* data is from 'bigquery-public-data' project

* the billing project will be charged for GCP services (here BigQuery)

* don't worry we will not exceed the free 1Tb ;)



## Querying data - naive way

```{r}
tbl(con, "health_nutrition_population") %>%
    collect(page_size = 10^6) %>%
    pull(indicator_name) %>% 
    unique()
```

* `collect()` explicitly downloads the result of a query

* `pull()` and `unique()` are processed locally




## Querying data - smart way

```{r}
qry2 <- tbl(con, "health_nutrition_population") %>%
    select(indicator_name) %>% 
    distinct() 

show_query(qry2)
```

* nothing is actually done so far!

* only the `SQL` query is generated!



## Explicitly `collect`-ing results

```{r}
collect(qry2)
```

* **implicitly called** when something cannot be translated

* **much more efficient** since 
    1. query is executed massively parallel in Google cloud
    2. only the result is sent via internet connection



## Your Turn! (individually, 15 min)

1) open binder **https://bit.ly/2U9rvJj** (4 min)

2) open `day-1/part-2/exercise-google-bigquery.R` (1 min)

3) configure the DB connection to your GCP project (5 min)

4) establish a connection and try both query variants (5 min)



# What about this NoSQL stuff?

* biggest 'database' - the world wide web

* highly unstructured, how can we query data?

* modern web services (database) communicate via **REST APIs**

* even BigQuery uses REST in the background!



## RESTful APIs 

* REST = [REpresentational State Transfer](https://en.wikipedia.org/wiki/Representational_state_transfer)

* communicate with web service via methods `GET`, `POST`, etc. and
URL strings

* we will only use `GET` via `httr` package to query data

* idea: tell server that you want something (and what)
```
GET https://server.address/what/I/want
```



## RESTful APIs - example

* World Bank data contains a ton of population based indicators by country ID (ISO alpha-3 standard)

* how can we link that with additional information on the countries to refine
analyses?

* need to 'join' with a second database

* e.g., https://restcountries.eu/#api-endpoints

```
GET https://restcountries.eu/rest/v2/currency/{currency}
GET https://restcountries.eu/rest/v2/currency/eur
```



## How it's done (1)

* query all EU countries 3-letter ISO names
```{r}
httr::GET('https://restcountries.eu/rest/v2/regionalbloc/eu')
```

* the result tells us that we got a JSON file back 

* could have been literally anything!

* need to parse it to get the data content



## How it's done (2)

* parse contents of response object (using JSON parser)
```{r, warning=FALSE, message=FALSE}
httr::GET('https://restcountries.eu/rest/v2/regionalbloc/eu') %>%
    httr::parsed_content() 
```

* top level is country

* need to extract `$alpha3Code` field



## How it's done (3)

* map `function(x) {x$alpha3Code}` to all elements of the list

* `sapply` could do the job 
```{r, warning=FALSE, message=FALSE}
httr::GET('https://restcountries.eu/rest/v2/regionalbloc/eu') %>%
    httr::parsed_content() %>%
    sapply(., function(x) x$alpha3Code) 
```
* problem: sapply does a lot of heuristics under the hood, easy to introduce
errors!



## `purrr` - type-safe functional programming

* <p> <img src="../../resources/hexstickers/purrr.png" align="right" width="200px"> better use `purrr::map` family of functions </p>

* want a character vector - use `map_chr` which guarantees that!

* `purr::map` functions also support shorthand notation for anonymous functions:
```
map(input, function(x) sin(x))
map(input, ~sin(.))
```

* essentially replaces for loops



## `purrr::map` vs. `base::sapply`

```{r, warning=FALSE, message=FALSE}
httr::GET('https://restcountries.eu/rest/v2/regionalbloc/eu') %>%
    httr::parsed_content() %>%
    sapply(., function(x) x$alpha3Code) 
```

```{r, warning=FALSE, message=FALSE}
httr::GET('https://restcountries.eu/rest/v2/regionalbloc/eu') %>%
    httr::parsed_content() %>%
    map_chr(., ~.$alpha3Code)
```



## Joining data: add Gini index

* https://restcountries.eu has Gini index per country
```{r, warning=FALSE, message=FALSE}
tbl_gini <- httr::GET('https://restcountries.eu/rest/v2/all') %>%
    httr::parsed_content() %>%
    {tibble(
        country_code = map_chr(., ~.$alpha3Code),
        gini         = map_dbl(., ~ifelse(is.null(.$gini), NA_real_, .$gini))
    )}
print(tbl_gini, n = 3)
```



## Extracting cross section of HNP data

* reduce World Bank data to closest date (up to 2010)
```{r}
tbl_wb_hnp_cross_sectional <- tbl(con, "health_nutrition_population") %>% 
    filter(year >= 2010) %>% 
    collect(page_size = 25000) %>% # using r function `which.max`
    group_by(country_code, indicator_code) %>%
    filter(year == year[which.max(year)]) %>% 
    ungroup() %>% 
    pivot_wider(
        c(country_name, country_code, value), 
        names_from = indicator_name, values_from = value
    )
print(tbl_wb_hnp_cross_sectional, n = 3)
```



## Data base joins

<img src="../../resources/figures/sql_joins.png" style="float:left; width:75%; margin-right:5%; margin-bottom:0.5em;">


## `Sorting-in` the Gini at the right place

* database 'joins' are extremely powerful (here 'left_join')
```{r}
left_join(
        tbl_wb_hnp_cross_sectional,
        tbl_gini,
        by = 'country_code'
    ) %>% 
    select(country_name, country_code, gini, everything())
```



## Exercise (individually, 15 min)

1) open binder **https://bit.ly/2U9rvJj** (4 min)

2) open `day-1/part-2/exercise-manual-rest-api.R` (1 min)

3) step through the code and try to understand what's happening (5 min)

4) Using the REST API try downloading only information for Germany and France
directly (https://restcountries.eu/#api-endpoints-list-of-codes) (5 min)



# Break (15 min)



# Advanced stuff

<img src="../../resources/figures/rocket.jpg" height="550px">



## Let's query smoking data by gender

```{r}
tbl_smoking_prevalence <- tbl(con, "health_nutrition_population") %>%
    filter(
        indicator_name %in% c(
            "Smoking prevalence, females (% of adults)",
            "Smoking prevalence, males (% of adults)"
        )
    ) %>% 
    mutate(gender = str_extract(indicator_name, "female|male")) %>% 
    select(-indicator_name, -indicator_code) %>% 
    arrange(country_code, year, gender) 
show_query(tbl_smoking_prevalence)
```



## Difference between query and tibble objects

```{r}
print(tbl_smoking_prevalence, n = 2)
tbl_smoking_prevalence <- collect(tbl_smoking_prevalence)
print(tbl_smoking_prevalence, n = 2)
```



## Let's look at an individual country

```{r, fig.height=3}
tbl_smoking_prevalence %>%
    filter(country_code == 'GBR') %>% 
    ggplot(aes(year, value, color = gender)) +
        geom_point() +
        geom_line() +
        ylim(c(0, NA))
```



## 'Quick-and-dirty' model

* do smoking habits change differently between genders?

```{r}
tbl_smoking_prevalence %>%
    filter(country_code == 'GBR') %>% 
    lm(value ~ year*gender, data = .) %>% 
    summary()
```



## How do we do that for **all** countries?

* for loop? 

* ugly, how do I find interesting models in the end?

* <p> <img src="../../resources/hexstickers/tibble.png" align="right" width="200px;"> want models and sumary in tabular structure - `tibble`! </p>

* <p> <img src="../../resources/hexstickers/purrr.png" align="right" width="200px;"> need to operate generically over columns to do that - functional programming concepts (`purrr` package) </p>



## Start by nesting the data for each country

```{r}
tbl_smoking_prevalence %>%
    group_by(country_code) %>% 
    filter(gender %>% unique %>% length == 2) %>% 
    nest()
```



## 'map' the `lm` function to each nested dataset

```{r}
tbl_smoking_models <- tbl_smoking_prevalence %>%
    group_by(country_name) %>% 
    filter(gender %>% unique %>% length == 2) %>% 
    nest() %>% 
    mutate(
        model = map(data, ~lm(value ~ year*gender, data = .))
    ) %>% 
    ungroup()

tbl_smoking_models
```



## Format model summary in tables - `broom`

<img src="../../resources/hexstickers/broom.png" width="200px">

```{r}
tbl_smoking_models$model[[1]] %>% 
    broom::glance()
```



## Format model predictions in tables - `broom`

```{r}
tbl_smoking_models$model[[1]] %>% 
    broom::augment()
```



## Format model parameters in tables - `broom`

```{r}
tbl_smoking_models$model[[1]] %>% 
    broom::tidy()
```


## `purrr::map` + `tibble` + `broom::glance` (1)

```{r}
tbl_smoking_models %>% 
    mutate(
        fit = map(model, broom::glance)
    )
```

## `purrr::map` + `tibble` + `broom::glance` (2)

```{r}
tbl_smoking_models %>% 
    mutate(
        fit = map(model, broom::glance)
    ) %>% 
    unnest(fit) %>% 
    print(n = Inf)
```


## Investigate model fit

```{r, fig.height=2.75, fig.width=11}
tbl_smoking_models %>% 
    mutate(fit = map(model, broom::glance)) %>% 
    unnest(fit) %>% 
    arrange(r.squared) %>% 
    head(n = 8) %>% # pick 8 worst
    select(country_name, data, model) %>% 
    mutate(augmented = map(model, broom::augment)) %>% 
    unnest(augmented) %>% 
    ggplot(aes(year, value, color = gender)) +
        geom_point() +
        geom_line(aes(y = .fitted)) + 
        ylim(c(0, 100)) +
        facet_wrap(~country_name, nrow = 1) +
        theme(
            axis.text.x = element_text(angle = 60, hjust = 1)
        )
```



## Biggest gender differences in change/year

```{r, fig.height=2.75, fig.width=11}
tbl_smoking_models %>% 
    mutate(parameters = map(model, broom::tidy)) %>% 
    unnest(parameters) %>% 
    filter(term == 'year:gendermale') %>% 
    arrange(desc(abs(estimate))) %>% 
    head(n = 8) %>% # extract 8 models with highest interaction term
    select(country_name, data, model) %>% 
    mutate(augmented = map(model, broom::augment)) %>% 
    unnest(augmented) %>% 
    ggplot(aes(year, value, color = gender)) +
        geom_point() + 
        geom_line(aes(y = .fitted)) + # .fitted is genrated by 
                                      # 'broom::augment'
        ylim(c(0, 100)) +
        facet_wrap(~country_name, nrow = 1) +
        theme(
            axis.text.x = element_text(angle = 60, hjust = 1)
        )
```



## Exercise (individual, 20 min)

1) **(5 min)** open binder **https://bit.ly/2U9rvJj** and `day-1/part-2/exercise-advanced-processing.R` 

2) **(15 min)** go through the individual steps and familiarize yourself with 
    * `tidyr::nest`, `tidyr::unnest`
    * `purrr::map`, `purrr::map_*`
    * `broom::glance`, `broom::augment`, and `broom::tidy`



# Your Turn! 

* tomorrow, you will learn essential tools for
    * making analyses reproducible 
    * creating fancy reports
    * collaborating with peers
    
* now, get together in groups ($n\approx 5$)!
    
* each group defines an independent mini-project using an online data source (be realistic)



## Task (groups, $\approx$ 1h)

* implement an anlysis all the way from querying the data via Google BigQuery
or zenodo.org

* work on one laptop and share results later (slack!)

* on binder: regularly backup/download your script!

* your projects serve as example tomorrow and on Saturday!

* continue hacking tomorrow

* Saturday morning: transform analysis to shiny app and present
